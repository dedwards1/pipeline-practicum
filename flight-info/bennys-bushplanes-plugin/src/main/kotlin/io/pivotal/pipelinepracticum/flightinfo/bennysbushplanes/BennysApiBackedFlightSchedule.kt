package io.pivotal.pipelinepracticum.flightinfo.bennysbushplanes

import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate
import java.time.Month

class BennysApiBackedFlightSchedule(
        private val baseUrl: String,
        private val restTemplate: RestTemplate
) : FlightSchedule {

    override fun fetch() = callService()
            .entries
            .flatMap { yearEntry ->
                yearEntry.value.entries
                        .flatMap { monthEntry ->
                            monthEntry.value.entries
                                    .flatMap { dayEntry ->
                                        dayEntry.value
                                                .map { flightNumber ->
                                                    Pair(flightNumber, dateFor(yearEntry.key, monthEntry.key, dayEntry.key))
                                                }
                                    }
                        }
            }
            .groupBy { it.second }
            .mapValues { entry -> entry.value.map { it.first } }

    private fun dateFor(yearEntry: Int, monthEntry: String, dayEntry: Int) =
            LocalDate.of(yearEntry, Month.valueOf(monthEntry).value, dayEntry)

    private fun callService() = restTemplate.exchange(
            RequestEntity<Any>(HttpMethod.GET, URI.create(baseUrl)),
            object : ParameterizedTypeReference<Map<Int, Map<String, Map<Int, List<String>>>>>() {}
    ).body ?: emptyMap()
}
