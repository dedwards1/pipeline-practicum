package io.pivotal.pipelinepracticum.flightinfo.bennysbushplanes

import com.fasterxml.jackson.annotation.JsonProperty
import io.pivotal.pipelinepracticum.flightstatus.FlightScheduleContract
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate

@RunWith(SpringRunner::class)
class BennysApiBackedFlightScheduleTest : FlightScheduleContract() {
    private val restTemplate = RestTemplate()
    private val schedule = BennysApiBackedFlightSchedule(baseUrl(), restTemplate)

    override fun objectUnderTest() = schedule

    override fun givenAFlight(flightNumber: String, flightDate: LocalDate) {

        restTemplate.postForLocation(
                URI.create(baseUrl()),
                FlightJson(
                        flightNumber,
                        flightDate.toString()
                )
        )
    }

    private fun baseUrl(): String {
        val url = System.getenv("ATC_TEST_URL")
        return if(url == null || url == "")
            "https://pipeline-practicum-air-traffic-control-transtracheal-ignominy.apps.pcfone.io/bennysbushplanes/flights"
        else
            "$url/bennysbushplanes/flights"
    }
}

private class FlightJson(
        @JsonProperty var flightNumber: String = "",
        @JsonProperty var date: String = ""
)

