package io.pivotal.pipelinepracticum.flightinfo.aleutianairlines

import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import io.pivotal.pipelinepracticum.flightstatus.FlightScheduleContract
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate

@RunWith(SpringRunner::class)
class ScheduleServiceBackedFlightScheduleTest : FlightScheduleContract() {
    private val restTemplate = RestTemplate()

    override fun objectUnderTest(): FlightSchedule {
        return ServiceBackedFlightSchedule(baseUrl(), restTemplate)

    }

    override fun givenAFlight(flightNumber: String, flightDate: LocalDate) {
        restTemplate.postForLocation(
                URI.create("${baseUrl()}/api/v1/schedule"), FlightJson(flightNumber, flightDate.toString())
        )
    }

    private fun baseUrl(): String {
        val url = System.getenv("SCHEDULE_TEST_URL")
        return if (url == null || url == "") "https://akta-air-schedule.apps.pcfone.io/" else url
    }
}
