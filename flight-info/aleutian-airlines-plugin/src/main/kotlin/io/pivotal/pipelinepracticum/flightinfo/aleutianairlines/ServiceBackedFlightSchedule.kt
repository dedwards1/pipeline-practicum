package io.pivotal.pipelinepracticum.flightinfo.aleutianairlines

import com.fasterxml.jackson.annotation.JsonProperty
import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate

class ServiceBackedFlightSchedule(val baseUrl: String, val restTemplate: RestTemplate) : FlightSchedule {
    override fun fetch(): Map<LocalDate, List<String>> {
        val apiResults = restTemplate.exchange(
                RequestEntity<Any>(HttpMethod.GET, URI.create("$baseUrl/api/v1/schedule")),
                object : ParameterizedTypeReference<Map<String, List<String>>>() {}
        ).body

        val schedule = HashMap<LocalDate, MutableList<String>>()
        apiResults?.entries?.forEach { entry ->
            val flightNumber = entry.key
            val dates = entry.value.map { LocalDate.parse(it) }
            dates.forEach { date ->
                if (schedule[date] == null) {
                    schedule[date] = mutableListOf(flightNumber)
                } else {
                    schedule[date]!!.add(flightNumber)
                }
            }
        }

        return schedule
    }

}


class FlightJson(
        @JsonProperty var number: String = "",
        @JsonProperty var date: String = ""
)