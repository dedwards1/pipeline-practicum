package io.pivotal.pipelinepracticum.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightUpdate

class FakeFlightStatusLookupTest: FlightStatusLookupContract() {
    val fakeLookup = FakeFlightStatusLookup()
    override fun lookup() = fakeLookup
    override fun givenAFlightUpdate(update: FlightUpdate) = fakeLookup.add(update)
}