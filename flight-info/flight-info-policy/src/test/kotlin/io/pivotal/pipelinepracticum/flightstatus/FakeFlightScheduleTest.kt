package io.pivotal.pipelinepracticum.flightstatus

import java.time.LocalDate

class FakeFlightScheduleTest : FlightScheduleContract() {

    private val schedule = FakeFlightSchedule()

    override fun objectUnderTest() = schedule

    override fun givenAFlight(flightNumber: String, flightDate: LocalDate) {
        schedule.add(flightNumber, flightDate)
    }

}