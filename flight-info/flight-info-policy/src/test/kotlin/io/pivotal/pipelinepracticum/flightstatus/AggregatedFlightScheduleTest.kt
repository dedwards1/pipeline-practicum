package io.pivotal.pipelinepracticum.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.AggregatedFlightSchedule
import junit.framework.Assert.fail
import org.junit.After
import java.time.LocalDate

class AggregatedFlightScheduleTest : FlightScheduleContract() {

    val sourceSchedule1 = FakeFlightSchedule()
    val sourceSchedule2 = FakeFlightSchedule()
    val aggregatedSchedule = AggregatedFlightSchedule(sourceSchedule1, sourceSchedule2)
    var addedFlights = 0

    override fun objectUnderTest() = aggregatedSchedule

    override fun givenAFlight(flightNumber: String, flightDate: LocalDate) {
        (if(addedFlights % 2 == 0) sourceSchedule1 else sourceSchedule2)
                .add(flightNumber, flightDate)

        addedFlights++
    }

    @After
    fun tearDown() {
        if(addedFlights < 2) {
            fail("For AggregatedFlightTest to be meaningfully tested, at least 2 flights need to be added.")
        }
    }
}