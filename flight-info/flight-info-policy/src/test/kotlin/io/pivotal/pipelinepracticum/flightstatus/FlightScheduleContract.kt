package io.pivotal.pipelinepracticum.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate

abstract class FlightScheduleContract {

    abstract fun objectUnderTest(): FlightSchedule

    abstract fun givenAFlight(flightNumber: String, flightDate: LocalDate)

    @Test
    fun `given some flights I can fetch their schedule`() {
        givenAFlight("TEST 900", LocalDate.of(2019, 2, 1))
        givenAFlight("TEST 900", LocalDate.of(2019, 2, 2))
        givenAFlight("TEST 901", LocalDate.of(2019, 2, 1))
        givenAFlight("TEST 901", LocalDate.of(2019, 2, 4))

        val flightSchedule: Map<LocalDate, List<String>> = objectUnderTest().fetch()

        // TODO: find a way to use containsExactlyInAnyOrder w/ real service that has messy data
        assertThat(flightSchedule[LocalDate.of(2019, 2, 1)]).contains("TEST 900", "TEST 901")
        assertThat(flightSchedule[LocalDate.of(2019, 2, 2)]).contains("TEST 900")
        assertThat(flightSchedule[LocalDate.of(2019, 2, 4)]).contains("TEST 901")
    }
}