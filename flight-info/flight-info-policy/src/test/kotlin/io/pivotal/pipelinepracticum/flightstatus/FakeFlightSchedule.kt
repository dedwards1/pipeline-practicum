package io.pivotal.pipelinepracticum.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import java.time.LocalDate

class FakeFlightSchedule : FlightSchedule {
    private val schedule : MutableMap<LocalDate, MutableList<String>> = HashMap()

    override fun fetch(): Map<LocalDate, List<String>> = schedule

    fun add(flightNumber: String, flightDate: LocalDate) {
        if (schedule[flightDate] == null) {
            schedule[flightDate] = mutableListOf(flightNumber)
        } else {
            schedule[flightDate]!!.add(flightNumber)
        }
    }
}
