package io.pivotal.pipelinepracticum.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.FlightUpdate
import java.time.LocalDate

class FakeFlightStatusLookup: FlightStatusLookup {
    val updates: MutableList<FlightUpdate> = mutableListOf()

    override fun updatesFor(date: LocalDate): List<FlightUpdate> {
        return updates.filter { it.date == date }
    }

    fun add(update: FlightUpdate) {
        updates.add(update)
    }
}