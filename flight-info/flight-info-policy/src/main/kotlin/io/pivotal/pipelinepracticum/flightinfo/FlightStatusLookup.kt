package io.pivotal.pipelinepracticum.flightinfo

import java.time.LocalDate

interface FlightStatusLookup {
    fun updatesFor(date: LocalDate): List<FlightUpdate>
}
