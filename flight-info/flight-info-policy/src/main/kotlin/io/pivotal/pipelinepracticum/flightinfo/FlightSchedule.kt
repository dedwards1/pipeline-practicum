package io.pivotal.pipelinepracticum.flightinfo

import java.time.LocalDate

interface FlightSchedule {
    fun fetch(): Map<LocalDate, List<String>>
}
