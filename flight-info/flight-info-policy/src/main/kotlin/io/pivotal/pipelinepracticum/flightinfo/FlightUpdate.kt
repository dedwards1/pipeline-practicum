package io.pivotal.pipelinepracticum.flightinfo

import java.time.LocalDate

data class FlightUpdate(val flightNumber: String, val date: LocalDate, val status: String)