package io.pivotal.pipelinepracticum.flightinfo

import java.time.LocalDate

class AggregatedFlightSchedule(private vararg val sources: FlightSchedule) : FlightSchedule {

    override fun fetch() = sources
            .map { it.fetch() }
            .flatMap { asFlightList(it) }
            .groupBy { it.first }
            .mapValues { entry -> entry.value.map { it.second } }

    private fun asFlightList(schedule: Map<LocalDate, List<String>>) =
            schedule.entries.flatMap { entry -> entry.value.map { flightNumber -> Pair(entry.key, flightNumber) } }

}
