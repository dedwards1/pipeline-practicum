package io.pivotal.pipelinepracticum.flightinfo.airtrafficcontrol

import com.fasterxml.jackson.annotation.JsonProperty
import io.pivotal.pipelinepracticum.flightinfo.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.FlightUpdate
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpMethod
import org.springframework.http.RequestEntity
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate

class ATCBackedFlightStatusLookup(val baseUrl: String, val restTemplate: RestTemplate) : FlightStatusLookup {
    override fun updatesFor(date: LocalDate): List<FlightUpdate> {
        return restTemplate.exchange(
                RequestEntity<Any>(HttpMethod.GET, URI.create("$baseUrl/statuses/$date")),
                object : ParameterizedTypeReference<List<FlightUpdateJson>>() {}
        ).body!!.map { FlightUpdate(it.flightNumber, LocalDate.parse(it.date), it.status) }
    }
}

class FlightUpdateJson(
        @JsonProperty var flightNumber: String = "",
        @JsonProperty var date: String = "",
        @JsonProperty var status: String = ""
)