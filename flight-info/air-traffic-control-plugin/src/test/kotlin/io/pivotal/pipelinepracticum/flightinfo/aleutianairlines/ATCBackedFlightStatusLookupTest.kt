package io.pivotal.pipelinepracticum.flightinfo.aleutianairlines

import io.pivotal.pipelinepracticum.flightinfo.airtrafficcontrol.ATCBackedFlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.airtrafficcontrol.FlightUpdateJson
import io.pivotal.pipelinepracticum.flightinfo.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightstatus.FlightStatusLookupContract
import io.pivotal.pipelinepracticum.flightinfo.FlightUpdate
import org.junit.runner.RunWith
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.web.client.RestTemplate
import java.net.URI

@RunWith(SpringRunner::class)
@ContextConfiguration(classes = [ATCBackedFlightStatusLookupTestConfig::class])
@ActiveProfiles("ATCBackedFlightStatusLookupTest")
class ATCBackedFlightStatusLookupTest: FlightStatusLookupContract() {

    val restTemplate = RestTemplate()

    override fun lookup(): FlightStatusLookup {
        return ATCBackedFlightStatusLookup(baseUrl(), restTemplate)
    }

    override fun givenAFlightUpdate(update: FlightUpdate) {
        restTemplate.postForLocation(
                URI.create("${baseUrl()}/status"), FlightUpdateJson(update.flightNumber, "${update.date}", update.status)
        )
    }

    private fun baseUrl(): String {
        val url = System.getenv("ATC_TEST_URL")
        return if(url == null || url == "") "https://pipeline-practicum-air-traffic-control-transtracheal-ignominy.apps.pcfone.io" else url
    }
}

@Configuration
@EnableAutoConfiguration
@Profile("ATCBackedFlightStatusLookupTest")
class ATCBackedFlightStatusLookupTestConfig
