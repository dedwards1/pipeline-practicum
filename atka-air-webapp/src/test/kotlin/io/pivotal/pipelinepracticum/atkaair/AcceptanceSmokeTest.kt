package io.pivotal.pipelinepracticum.atkaair

import io.pivotal.pipelinepracticum.flightinfo.aleutianairlines.FlightJson
import io.pivotal.pipelinepracticum.flightinfo.airtrafficcontrol.FlightUpdateJson
import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import io.pivotal.pipelinepracticum.journeys.FlightSubscriptionsJourney
import org.junit.experimental.categories.Category
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.time.LocalDate

@Category(AcceptanceSmokeTest::class)
class AcceptanceSmokeTest : FlightSubscriptionsJourney() {
    override fun baseUrl() = "https://atka-air-gadoid-regeneracy.apps.pcfone.io/"

    override fun givenSomeFlightsWithStatuses(): Map<Flight, String> {
        val flightsWithStatus = mapOf(
                Flight("KS 991", LocalDate.of(2018, 11, 30)) to "Delayed",
                Flight("KS 992", LocalDate.of(2018, 12, 31)) to "On Time"
        )

        val restTemplate = RestTemplate()
        flightsWithStatus.entries.forEach {
            restTemplate.postForLocation(
                    URI.create("${scheduleServiceBaseUrl()}/api/v1/schedule"), FlightJson(it.key.flightNumber, it.key.date.toString())
            )

            restTemplate.postForLocation(
                    URI.create("${atcServiceBaseUrl()}/status"), FlightUpdateJson(it.key.flightNumber, "${it.key.date}", it.value)
            )
        }

        return flightsWithStatus
    }

    private fun scheduleServiceBaseUrl(): String {
        val url = System.getenv("SCHEDULE_TEST_URL")
        return if (url == null || url == "") "https://akta-air-schedule.apps.pcfone.io/" else url
    }

    private fun atcServiceBaseUrl(): String {
        val url = System.getenv("ATC_TEST_URL")
        return if (url == null || url == "") "https://pipeline-practicum-air-traffic-control-transtracheal-ignominy.apps.pcfone.io" else url
    }
}