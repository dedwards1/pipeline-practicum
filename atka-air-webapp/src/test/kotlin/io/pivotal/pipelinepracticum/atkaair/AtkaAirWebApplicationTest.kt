package io.pivotal.pipelinepracticum.atkaair

import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
@TestPropertySource(properties = [
    "ATC_SERVICE_URL=http://www.example.com",
    "SCHEDULE_SERVICE_URL=http://www.example.com"
])
class AtkaAirWebApplicationTest {

    @Test
    fun `the application can successfully start with available configuration`() {
    }

}
