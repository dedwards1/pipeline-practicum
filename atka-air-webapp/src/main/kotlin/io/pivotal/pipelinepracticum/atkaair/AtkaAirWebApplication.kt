package io.pivotal.pipelinepracticum.atkaair

import io.pivotal.pipelinepracticum.flightinfo.AggregatedFlightSchedule
import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import io.pivotal.pipelinepracticum.flightinfo.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.airtrafficcontrol.ATCBackedFlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.aleutianairlines.ServiceBackedFlightSchedule
import io.pivotal.pipelinepracticum.flightinfo.bennysbushplanes.BennysApiBackedFlightSchedule
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribeToFlight
import io.pivotal.pipelinepracticum.flightsubscriptions.db.DbBackedPassengerRepository
import io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus.GetAvailableFlightsFromFlightSchedule
import io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus.GetFlightStatusesForSubscribedFlights
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.web.client.RestTemplate

@SpringBootApplication
@ComponentScan("io.pivotal.pipelinepracticum.flightsubscriptions.web")
@EnableJpaRepositories("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@EntityScan("io.pivotal.pipelinepracticum.flightsubscriptions.db")
@Import(
        DbBackedPassengerRepository::class,
        SubscribeToFlight::class,
        GetFlightStatusesForSubscribedFlights::class,
        GetAvailableFlightsFromFlightSchedule::class
)
class AtkaAirWebApplication {
    @Bean
    fun flightStatusLookup(
            @Value("\${ATC_SERVICE_URL}") atcServiceUrl: String,
            restTemplate: RestTemplate
    ): FlightStatusLookup {
        return ATCBackedFlightStatusLookup(atcServiceUrl, restTemplate)
    }

    @Bean
    fun flightSchedule(
            @Value("\${SCHEDULE_SERVICE_URL}") scheduleServiceUrl: String,
            @Value("\${ATC_SERVICE_URL}") atcServiceUrl: String,
            restTemplate: RestTemplate
    ): FlightSchedule {
        return AggregatedFlightSchedule(
                ServiceBackedFlightSchedule(scheduleServiceUrl, restTemplate),
                BennysApiBackedFlightSchedule("$atcServiceUrl/bennysbushplanes/flights", restTemplate)
        )
    }

    @Bean
    fun restTemplate() = RestTemplate()
}

fun main(args: Array<String>) {
    runApplication<AtkaAirWebApplication>(*args)
}
