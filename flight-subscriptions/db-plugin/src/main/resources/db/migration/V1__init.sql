CREATE TABLE passenger (
	id BIGINT NOT NULL AUTO_INCREMENT,
	name varchar(255) not null,
	subscriptions varchar(255) not null,
	PRIMARY KEY (id)
);