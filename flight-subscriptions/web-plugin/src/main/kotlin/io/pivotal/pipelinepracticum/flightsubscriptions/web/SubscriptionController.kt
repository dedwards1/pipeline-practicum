package io.pivotal.pipelinepracticum.flightsubscriptions.web

import io.pivotal.pipelinepracticum.flightsubscriptions.*
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import java.time.LocalDate

@Controller
class SubscriptionController(
        val passengerRepository: PassengerRepository,
        val subscribeToFlight: SubscribeToFlight,
        val subscribedFlightStatuses: SubscribedFlightStatuses,
        val availableFlightsLookup: AvailableFlightsLookup
) {

    @GetMapping("/flight-subscription")
    fun newSubscription(model: Model): String {
        model.addAttribute("passengers", passengerRepository.findAllPassengers())
        model.addAttribute("availableDates", availableFlightsLookup.availableDates().map { it.toString() })
        return "new-subscription"
    }

    @PostMapping("/flight-subscription/choose-date")
    fun chooseDate(
            @RequestParam("passenger_id") passengerId: String,
            @RequestParam("date") date: String,
            model: Model
    ): String {
        model.addAttribute("passengerId", passengerId)
        model.addAttribute("date", date)
        model.addAttribute(
                "availableFlights",
                availableFlightsLookup.availableFlightsOn(LocalDate.parse(date)).map { it.flightNumber }
        )
        return "choose-flight"
    }

    @PostMapping("/flight-subscription")
    fun createSubscription(
            @RequestParam("passenger_id") passengerId: String,
            @RequestParam("flight_number") flightNumber: String,
            @RequestParam("date") date: String

    ) = subscribeToFlight.execute(flightNumber, LocalDate.parse(date), passengerId, object : SubscribeToFlight.Outcome<String> {
        override fun successfullySubscribed(updatedPassenger: Passenger) = "index"
        override fun noSuchPassenger(passengerId: String) = "error"
    })

    @GetMapping("/flight-subscriptions")
    fun flightSubscriptionsIndex(model: Model): String {
        model.addAttribute("passengers", passengerRepository.findAllPassengers())
        return "flight-subscriptions-passenger-select"
    }

    @GetMapping("/flight-subscriptions/{passengerId}")
    fun flightSubscriptionsForPassenger(@PathVariable("passengerId") passengerId: String, model: Model): String {
        val passenger = passengerRepository.findPassengerById(passengerId)!!
        val updates = subscribedFlightStatuses.forPassenger(passenger).entries

        model.addAttribute("updates", updates)
        model.addAttribute("passengerName", passenger.name)

        return "flight-subscriptions-for-passenger"
    }
}