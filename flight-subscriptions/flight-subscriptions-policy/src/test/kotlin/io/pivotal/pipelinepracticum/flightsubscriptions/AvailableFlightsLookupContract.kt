package io.pivotal.pipelinepracticum.flightsubscriptions

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate

abstract class AvailableFlightsLookupContract {
    abstract fun givenAFlight(flightNumber: String, date: LocalDate)

    abstract fun objectUnderTest(): AvailableFlightsLookup

    @Test
    fun `given available flights I can fetch the list of dates with flights`() {
        givenAFlight("KS 900", LocalDate.of(2019, 2, 1))
        givenAFlight("KS 900", LocalDate.of(2019, 2, 2))
        givenAFlight("KS 901", LocalDate.of(2019, 2, 1))
        givenAFlight("KS 901", LocalDate.of(2019, 2, 4))

        val dates = objectUnderTest().availableDates()

        assertThat(dates).containsExactlyInAnyOrder(
                LocalDate.of(2019, 2, 1),
                LocalDate.of(2019, 2, 2),
                LocalDate.of(2019, 2, 4))
    }

    @Test
    fun `given available flights I can fetch the list of available flights on a given date`() {
        givenAFlight("KS 900", LocalDate.of(2019, 2, 1))
        givenAFlight("KS 900", LocalDate.of(2019, 2, 2))
        givenAFlight("KS 901", LocalDate.of(2019, 2, 1))
        givenAFlight("KS 901", LocalDate.of(2019, 2, 4))

        val flightsOnTheFirst = objectUnderTest().availableFlightsOn(LocalDate.of(2019, 2, 1))

        assertThat(flightsOnTheFirst).containsExactlyInAnyOrder(
                Flight("KS 900", LocalDate.of(2019, 2, 1)),
                Flight("KS 901", LocalDate.of(2019, 2, 1))
        )
    }

}