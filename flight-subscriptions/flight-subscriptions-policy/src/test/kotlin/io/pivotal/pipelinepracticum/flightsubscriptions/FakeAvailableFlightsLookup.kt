package io.pivotal.pipelinepracticum.flightsubscriptions

import java.time.LocalDate

class FakeAvailableFlightsLookup : AvailableFlightsLookup {
    private val flights: MutableList<Flight> = mutableListOf()

    override fun availableFlightsOn(date: LocalDate): List<Flight> {
        return flights.filter { it.date == date }
    }

    override fun availableDates() = HashSet(flights.map { it.date }).toList()

    fun add(flightNumber: String, date: LocalDate) = flights.add(Flight(flightNumber = flightNumber, date = date))
}
