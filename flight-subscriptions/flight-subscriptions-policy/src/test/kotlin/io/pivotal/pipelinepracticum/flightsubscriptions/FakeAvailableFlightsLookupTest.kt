package io.pivotal.pipelinepracticum.flightsubscriptions

import java.time.LocalDate

class FakeAvailableFlightsLookupTest : AvailableFlightsLookupContract() {
    private val lookup = FakeAvailableFlightsLookup()

    override fun givenAFlight(flightNumber: String, date: LocalDate) {
        lookup.add(flightNumber, date)
    }

    override fun objectUnderTest(): AvailableFlightsLookup {
        return lookup
    }
}