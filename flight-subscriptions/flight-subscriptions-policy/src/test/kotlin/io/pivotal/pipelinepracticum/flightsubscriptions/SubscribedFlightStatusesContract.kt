package io.pivotal.pipelinepracticum.flightsubscriptions

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.time.LocalDate

abstract class SubscribedFlightStatusesContract {
    abstract fun givenAStatus(flight: Flight, status: String)

    @Test
    fun `looking up statuses for a passenger's subscribed flights`() {
        val firstFlight = Flight("KS 244", LocalDate.of(2018, 11, 5))
        val secondFlight = Flight("KS 244", LocalDate.of(2018, 11, 12))
        val thirdFlight = Flight("KS 246", LocalDate.of(2018, 11, 13))

        givenAStatus(firstFlight, "On Time")
        givenAStatus(secondFlight, "Delayed")

        val passenger = Passenger(name = "Chaalix", subscriptions = listOf(firstFlight, secondFlight, thirdFlight))

        val statuses = subscribedFlightStatuses().forPassenger(passenger)

        assertThat(statuses[firstFlight]).isEqualTo("On Time")
        assertThat(statuses[secondFlight]).isEqualTo("Delayed")
        assertThat(statuses[thirdFlight]).isEqualTo("(no update)")
    }

    abstract fun subscribedFlightStatuses(): SubscribedFlightStatuses
}