package io.pivotal.pipelinepracticum.flightsubscriptions

import java.time.LocalDate

interface AvailableFlightsLookup {
    fun availableDates(): List<LocalDate>
    fun availableFlightsOn(date: LocalDate): List<Flight>
}
