package io.pivotal.pipelinepracticum.flightsubscriptions

import java.time.LocalDate

data class Flight(val flightNumber: String, val date: LocalDate)