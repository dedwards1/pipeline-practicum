package io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus

import io.pivotal.pipelinepracticum.flightstatus.FakeFlightStatusLookup
import io.pivotal.pipelinepracticum.flightinfo.FlightUpdate
import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribedFlightStatusesContract

class GetFlightStatusesForSubscribedFlightsTest: SubscribedFlightStatusesContract() {
    val statusLookup: FakeFlightStatusLookup = FakeFlightStatusLookup()
    val plugin = GetFlightStatusesForSubscribedFlights(statusLookup)

    override fun givenAStatus(flight: Flight, status: String) {
        statusLookup.add(FlightUpdate(flight.flightNumber, flight.date, status))
    }

    override fun subscribedFlightStatuses() = plugin
}
