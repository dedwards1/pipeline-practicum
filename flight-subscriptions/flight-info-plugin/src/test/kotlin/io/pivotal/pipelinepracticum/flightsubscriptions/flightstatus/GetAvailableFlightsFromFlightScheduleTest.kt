package io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus

import io.pivotal.pipelinepracticum.flightstatus.FakeFlightSchedule
import io.pivotal.pipelinepracticum.flightsubscriptions.AvailableFlightsLookup
import io.pivotal.pipelinepracticum.flightsubscriptions.AvailableFlightsLookupContract
import java.time.LocalDate

class GetAvailableFlightsFromFlightScheduleTest() : AvailableFlightsLookupContract() {
    val flightSchedule = FakeFlightSchedule()
    val plugin = GetAvailableFlightsFromFlightSchedule(flightSchedule)

    override fun givenAFlight(flightNumber: String, date: LocalDate) {
        flightSchedule.add(flightNumber, date)
    }

    override fun objectUnderTest(): AvailableFlightsLookup {
        return plugin
    }
}
