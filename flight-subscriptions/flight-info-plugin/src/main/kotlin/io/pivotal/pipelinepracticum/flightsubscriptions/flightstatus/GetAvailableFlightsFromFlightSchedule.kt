package io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightSchedule
import io.pivotal.pipelinepracticum.flightsubscriptions.AvailableFlightsLookup
import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import java.time.LocalDate

class GetAvailableFlightsFromFlightSchedule(val flightSchedule: FlightSchedule) : AvailableFlightsLookup {
    override fun availableDates(): List<LocalDate> {
        return flightSchedule.fetch().keys.toList()
    }

    override fun availableFlightsOn(date: LocalDate): List<Flight> {
        val flightNumbers = flightSchedule.fetch().get(date)
        if (flightNumbers != null) {
            return flightNumbers.map { Flight(flightNumber = it, date = date) }
        }
        return emptyList()
    }
}
