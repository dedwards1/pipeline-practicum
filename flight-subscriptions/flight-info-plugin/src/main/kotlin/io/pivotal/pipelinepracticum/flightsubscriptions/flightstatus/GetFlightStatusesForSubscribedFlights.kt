package io.pivotal.pipelinepracticum.flightsubscriptions.flightstatus

import io.pivotal.pipelinepracticum.flightinfo.FlightStatusLookup
import io.pivotal.pipelinepracticum.flightsubscriptions.Flight
import io.pivotal.pipelinepracticum.flightsubscriptions.Passenger
import io.pivotal.pipelinepracticum.flightsubscriptions.SubscribedFlightStatuses
import java.time.LocalDate

class GetFlightStatusesForSubscribedFlights (val statusLookup: FlightStatusLookup) : SubscribedFlightStatuses {
    override fun forPassenger(passenger: Passenger): Map<Flight, String> {

        val datesWithFlights = passenger
                .subscriptions
                .fold(emptySet<LocalDate>()) { dates, flight ->
                    dates.plus(flight.date)
                }

        val statusesByDate = datesWithFlights.associate { date -> Pair(date, statusLookup.updatesFor(date)) }

        return passenger.subscriptions.associate { flight ->
            Pair(
                    flight,
                    statusesByDate
                            [flight.date]
                            ?.find { update -> update.flightNumber == flight.flightNumber }
                            ?.status
                            ?: "(no update)"
            )
        }
    }
}
